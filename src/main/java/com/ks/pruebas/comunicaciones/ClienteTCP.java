package com.ks.pruebas.comunicaciones;

import com.ks.tcp.Cliente;
import com.ks.tcp.EventosTCP;
import com.ks.tcp.Tcp;

/**
 * Created by migue on 09/11/2016.
 */
public class ClienteTCP extends Cliente implements EventosTCP
{
    public ClienteTCP()
    {
        setEventos(this);
    }

    public void conexionEstablecida(Cliente cliente)
    {
        System.out.println("Se conecto un usuario");
        cliente.enviar("Bienvenido");
    }

    public void errorConexion(String s)
    {

    }

    public void datosRecibidos(String s, byte[] bytes, Tcp tcp)
    {
        System.out.println(tcp.toString() + " - " + s);
    }

    public void cerrarConexion(Cliente cliente)
    {

    }
}
