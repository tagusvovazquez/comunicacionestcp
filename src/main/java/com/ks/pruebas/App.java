package com.ks.pruebas;


import com.ks.pruebas.comunicaciones.ClienteTCP;
import com.ks.pruebas.comunicaciones.ServidorTCP;

public class App
{
    public static void main(String[] args)
    {
        int VLintI = 0;

        ServidorTCP VLobjServer = new ServidorTCP();
        VLobjServer.setPuerto(6000);
        try
        {
            VLobjServer.conectar();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        for (VLintI = 0; VLintI < 3; VLintI++)
        {
            ClienteTCP VLobjCliente = new ClienteTCP();
            VLobjCliente.setIP("localhost");
            VLobjCliente.setPuerto(8000 + 1000*VLintI);
            try
            {
                VLobjCliente.conectar();
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
        }
    }
}
